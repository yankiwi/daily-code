// Daily Coding Problem: Problem #138 [Hard]
//
// Find the minimum number of coins required to make n cents.
//
// You can use standard American denominations, that is, 1¢, 5¢, 10¢, and 25¢.
//
// For example, given n = 16, return 3 since we can make it with a 10¢, a 5¢, and a 1¢.

import 'dart:io';

bool debug = false;

void main(){
  print("Enter number of cents: ");
  int n = int.parse(stdin.readLineSync());

  int quarter = (n ~/ 25);
  if(debug) print(quarter);
  if(quarter >= 1){  // find quarters
    if (quarter == 1) print("$quarter quarter.");
    if (quarter > 1) print("$quarter quarters.");
    n = (n % 25);
    if(debug) print(n);
  }

  int dime = (n ~/ 10);
  if(debug) print(dime);
  if(dime >= 1){  // find dimes
    if (dime == 1) print("$dime dime.");
    if (dime > 1) print("$dime dimes.");
    n = (n % 10);
    if(debug) print(n);
  }

  int nickel = (n ~/ 5);
  if(debug) print(nickel);
  if(nickel >= 1){  // find nickel
    if (nickel == 1) print("$nickel nickel.");
    if (nickel > 1) print("$nickel nickels.");
    n = (n % 5);
    if(debug) print(n);
  }

  int penny = (n ~/ 1);
  if(debug) print(penny);
  if(penny >= 1){  // find penny
    if (penny == 1) print("$penny penny.");
    if (penny > 1) print("$penny pennies.");
    n = (n % 1);
    if(debug) print(n);
  }

}