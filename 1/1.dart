// Daily Coding Problem: Problem #1
//
// This problem was recently asked by Google.
// 
// Given a list of numbers and a number k, return whether any two numbers from the
// list add up to k.
// 
// For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.
// 
// Bonus: Can you do this in one pass?

bool debug = false;

List<int> list = [10, 15, 3, 7];
int k = 17;

void main(){
  print("list: $list");
  print("sum: $k\n");

  bool result = false;
  int position = 1;

  for(int item in list){
    List<int> temp_list = [];
    if(debug) print("for item: $item");
    if(debug) print("position: $position");
    temp_list.addAll(list.take(position - 1));
    temp_list.addAll(list.skip(position));
    if(debug) print("temp_list: $temp_list");
    for(int temp_item in temp_list){
      if((item + temp_item) == k){
        print("$item + $temp_item = $k");
        result = true;
      } 
    }
    position++;
  }
  print("\nResult: $result");
}