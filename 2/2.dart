// Daily Coding Problem: Problem #2
//
// Given an array of integers, return a new array such that each element at index i
// of the new array is the product of all the numbers in the original array except
// the one at i.
//
// For example, if our input was [1, 2, 3, 4, 5], the expected output would be
// [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be
// [2, 3, 6].
//
// Follow-up: what if you can't use division?

bool debug = false;

//List<int> list = [1, 2, 3, 4, 5];
List<int> list = [3, 2, 1];

void main(){
  print("without div: ${without_div(list)}");
  print("with div: ${with_div(list)}");
}

List<int> without_div(List<int> list){
  int position = 1;
  List<int> result = [];

  for(int item in list){
    List<int> inner_list = [];
    if(debug) print("for item: $item");
    if(debug) print("position: $position");
    inner_list.addAll(list.take(position - 1));
    inner_list.addAll(list.skip(position));
    if(debug) print("inner_list: $inner_list");
    int inner_pos = 0;
    int product = 0;
    for(int inner_item in inner_list){
      if(debug) print("inner_item: $inner_item");
      if(inner_pos == 0) product = inner_item;
      else product = product * inner_item;
      if(debug) print("building product: $product");
      inner_pos++;
    }
    result.add(product);
    position++;
  }
  return result;
}

List<int> with_div(List<int> list){
  List<int> result = [];

  for(int item in list){
    if(debug) print("for item: $item");
    int product = 0;
    int position = 0;
    for(int inner_item in list){
      if(debug) print("position: $position");
      if(position == 0) product = inner_item;
      else product = product * inner_item;
      if(debug) print("building product: $product");
      position++;
    }
    product = product ~/ item;
    if(debug) print("building product: $product");
    result.add(product);
  }
  return result;
}